#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Import this for pudb debugger"""
# from pudb import set_trace
# set_trace()


class Volume:
    def __init__(self, max_volume):
        self._current_volume = 0
        self._max_volume = max_volume

    def add(self, volume):
        if self._current_volume + volume >= self._max_volume:
            diff = self._max_volume - self._current_volume
            self._current_volume = self._max_volume
            return diff
        if self._current_volume + volume <= 0:
            self._current_volume = 0
            return volume
        else:
            self._current_volume += volume
            return volume

    def fill(self):
        self._current_volume = self._max_volume

    def empty(self):
        self._current_volume = 0


class Methods:
    def __init__(self, volume1, volume2, need):
        self._need = need
        self._volume1 = volume1
        self._volume2 = volume2

        self._actions = ""
        self._iteration = 0
        self._found = False

    def run_brute_force(self):
        print "-*- Метод грубой силы -*-"
        self._actions = ""
        self._iteration = 0
        self._found = False
        self._volume1._current_volume = 0
        self._volume2._current_volume = 0
        self._method_brute_force(0)

    def run_in_depth(self):
        print "-*- Поиск в глубину -*-"
        self._actions = ""
        self._iteration = 0
        self._found = False
        self._volume1._current_volume = 0
        self._volume2._current_volume = 0
        self._method_in_depth(0)

    def run_a_star(self):
        print "-*- Поиск A* -*-"
        self._actions = ""
        self._iteration = 0
        self._found = False
        self._volume1._current_volume = 0
        self._volume2._current_volume = 0
        self._method_a_star(0)

    def _method_brute_force(self, deep):
        """
        Brute Force method :
        recurrently invokes itself while not
        get to the limiting deep
        thus all the possible variants will
        be covered

        """
        if self._iteration > 5000:
            print "Too zaloopleno"
            return
        if not self._found:
            deep += 1
            if deep < 10:
                # заполнить 1, перелить во 2
                value1 = self._volume1._current_volume
                value2 = self._volume2._current_volume
                prev_actions = self._actions

                self._actions += (
                    "заполнить 1, перелить во 2 : " +
                    "[1] : " + str(self._volume1._max_volume) + " => " +
                    "[2] : " + str(self._volume2._current_volume) + "\n"
                )

                self._volume1.fill()
                can_fill = self._volume2.add(self._volume1._current_volume)
                self._volume1.add(-can_fill)

                self._method_brute_force(deep)

                self._volume1._current_volume = value1
                self._volume2._current_volume = value2
                self._actions = prev_actions
                self._iteration += 1

                # transfuse from 1 to 2
                value1 = self._volume1._current_volume
                value2 = self._volume2._current_volume
                prev_actions = self._actions

                self._actions += (
                    "перелить из 1 в 2 : " +
                    "[1] : " + str(self._volume1._current_volume) + " => " +
                    "[2] : " + str(self._volume2._current_volume) + "\n"
                )

                can_fill = self._volume2.add(self._volume1._current_volume)
                self._volume1.add(-can_fill)

                self._method_brute_force(deep)

                self._volume1._current_volume = value1
                self._volume2._current_volume = value2
                self._actions = prev_actions
                self._iteration += 1

                # fill 2, transfuse to 1
                value1 = self._volume1._current_volume
                value2 = self._volume2._current_volume
                prev_actions = self._actions

                self._actions += (
                    "fill 2, transfuse to 1 : " +
                    "[2] : " + str(self._volume2._max_volume) + " => " +
                    "[1] : " + str(self._volume1._current_volume) + "\n"
                )

                self._volume2.fill()
                can_fill = self._volume1.add(self._volume2._current_volume)
                self._volume2.add(-can_fill)

                self._method_brute_force(deep)

                self._volume1._current_volume = value1
                self._volume2._current_volume = value2
                self._actions = prev_actions
                self._iteration += 1

                # transfuse from 2 to 1
                value1 = self._volume1._current_volume
                value2 = self._volume2._current_volume
                prev_actions = self._actions

                self._actions += (
                    "перелить из 2 в 1 : " +
                    "[2] : " + str(self._volume2._current_volume) + " => " +
                    "[1] : " + str(self._volume1._current_volume) + "\n"
                )

                can_fill = self._volume1.add(self._volume2._current_volume)
                self._volume2.add(-can_fill)

                self._method_brute_force(deep)

                self._volume1._current_volume = value1
                self._volume2._current_volume = value2
                self._actions = prev_actions
                self._iteration += 1

                # empty self._volume1
                if self._volume1._current_volume != 0:
                    value1 = self._volume1._current_volume
                    value2 = self._volume2._current_volume
                    prev_actions = self._actions

                    self._actions += (
                        "опустошить 1 : " +
                        "[1] : " + str(self._volume1._current_volume) +
                        "[2] : " + str(self._volume2._current_volume) + "\n"
                    )

                    self._volume1.empty()

                    self._method_brute_force(deep)

                    self._volume1._current_volume = value1
                    self._volume2._current_volume = value2
                    self._actions = prev_actions
                    self._iteration += 1

                # empty volume2
                if self._volume2._current_volume != 0:
                    value1 = self._volume1._current_volume
                    value2 = self._volume2._current_volume
                    prev_actions = self._actions

                    self._actions += (
                        "опустошить 2 : " +
                        "[1] : " + str(self._volume1._current_volume) +
                        "[2] : " + str(self._volume2._current_volume) + "\n"
                    )
                    self._volume2.empty()

                    self._method_brute_force(deep)

                    self._volume1._current_volume = value1
                    self._volume2._current_volume = value2
                    self._actions = prev_actions
                    self._iteration += 1

                self._method_brute_force(deep)
                self._iteration += 1
            else:
                if ((self._volume1._current_volume == need or
                        self._volume2._current_volume == need) and
                        not self._found):
                    if self._volume1._current_volume == need:
                        self._actions += (
                            "Кувшин 1 удовлетворяет требованиям: " +
                            str(need) +
                            "\n"
                        )
                    if self._volume2._current_volume == need:
                        self._actions += (
                            "Кувшин 2 удовлетворяет требованиям: " +
                            str(need) +
                            "\n"
                        )
                    self._actions += (
                        "Метод грубой силы количество итераций: " +
                        str(self._iteration)
                    )
                    print self._actions
                    self._found = True

    def _check(self):
        """
        Accessory function for depth-first search
        method.

        """
        if not self._found:
            if self._volume1._current_volume == need:
                self._actions += ("Кувшин 1 удовлетворяет требованиям: " +
                                  str(need) + "\n")
            if self._volume2._current_volume == need:
                self._actions += ("Кувшин 2 удовлетворяет требованиям: " +
                                  str(need) + "\n")
            if (self._volume1._current_volume == need or
                    self._volume2._current_volume == need):
                self._actions += (
                    "Поиск в глубину количество итераций: " +
                    str(self._iteration) +
                    "\n"
                )
                print self._actions
                return True
            else:
                return False
        else:
            return True

    def _method_in_depth(self, step):
        """
        Method recurrently invokes itself, while result
        not found or not reaches limited depth.

        """
        step += 1
        if not self._found and step < 10:
            # заполнить 1, перелить во 2
            value1 = self._volume1._current_volume
            value2 = self._volume2._current_volume
            prev_actions = self._actions

            self._actions += (
                "заполнить 1, перелить во 2 : " +
                "[1] : " + str(self._volume1._max_volume) + " => " +
                "[2] : " + str(self._volume2._current_volume) + "\n"
            )

            self._volume1.fill()
            can_fill = self._volume2.add(self._volume1._current_volume)
            self._volume1.add(-can_fill)

            self._iteration += 1

            if self._check():
                self._found = True
                return

            self._method_in_depth(step)

            self._volume1._current_volume = value1
            self._volume2._current_volume = value2
            self._actions = prev_actions

            # transfuse from 1 to 2
            if self._volume1._current_volume != 0:
                value1 = self._volume1._current_volume
                value2 = self._volume2._current_volume
                prev_actions = self._actions

                self._actions += (
                    "перелить из 1 в 2 : " +
                    "[1] : " + str(self._volume1._current_volume) + " => " +
                    "[2] : " + str(self._volume2._current_volume) + "\n"
                )

                can_fill = self._volume2.add(self._volume1._current_volume)
                self._volume1.add(-can_fill)

                self._iteration += 1

                if self._check():
                    self._found = True
                    return

                self._method_in_depth(step)

                self._volume1._current_volume = value1
                self._volume2._current_volume = value2
                self._actions = prev_actions

                # fill 2, transfuse to 1
                value1 = self._volume1._current_volume
                value2 = self._volume2._current_volume
                prev_actions = self._actions

                self._actions += (
                    "fill 2, transfuse to 1 : " +
                    "[2] : " + str(self._volume2._max_volume) + " => " +
                    "[1] : " + str(self._volume1._current_volume) + "\n"
                )

                self._volume2.fill()
                can_fill = self._volume1.add(self._volume2._current_volume)
                self._volume2.add(-can_fill)

                self._iteration += 1

                if self._check():
                    self._found = True
                    return

                self._method_in_depth(step)

                self._volume1._current_volume = value1
                self._volume2._current_volume = value2
                self._actions = prev_actions
                self._iteration += 1

                # transfuse from 2 to 1
                if self._volume2._current_volume != 0:
                    value1 = self._volume1._current_volume
                    value2 = self._volume2._current_volume
                    prev_actions = self._actions

                    self._actions += (
                        "перелить из 2 в 1 : " +
                        "[2] : " + str(self._volume2._current_volume) + " => " +
                        "[1] : " + str(self._volume1._current_volume) + "\n"
                    )

                    can_fill = self._volume1.add(self._volume2._current_volume)
                    self._volume2.add(-can_fill)

                    self._iteration += 1
                    if self._check():
                        self._found = True
                        return

                    self._method_in_depth(step)

                    self._volume1._current_volume = value1
                    self._volume2._current_volume = value2
                    self._actions = prev_actions

                # empty volume1
                if self._volume1._current_volume != 0:
                    value1 = self._volume1._current_volume
                    value2 = self._volume2._current_volume
                    prev_actions = self._actions

                    self._actions += (
                        "опустошить 1 : " +
                        "[1] : " + str(self._volume1._current_volume) +
                        "[2] : " + str(self._volume2._current_volume) + "\n"
                    )

                    self._volume1.empty()

                    self._iteration += 1

                    if self._check():
                        self._found = True
                        return

                    self._method_in_depth(step)

                    self._volume1._current_volume = value1
                    self._volume2._current_volume = value2
                    self._actions = prev_actions

                # empty self._volume2
                if self._volume2._current_volume != 0:
                    value1 = self._volume1._current_volume
                    value2 = self._volume2._current_volume
                    prev_actions = self._actions

                    self._actions += (
                        "опустошить 2 : " +
                        "[1] : " + str(self._volume1._current_volume) +
                        "[2] : " + str(self._volume2._current_volume) + "\n"
                    )

                    self._volume2.empty()

                    self._iteration += 1

                    if self._check():
                        self._found = True
                        return

                    self._method_in_depth(step)

                    self._volume1._current_volume = value1
                    self._volume2._current_volume = value2
                    self._actions = prev_actions

    def _a_check(self):
        """
        Accessory function for depth-first search
        method.

        """
        if (not self._found and (
                self._volume1._current_volume == need or
                self._volume2._current_volume == need)):
            if self._volume1._current_volume == need:
                self._actions += ("Кувшин 1 удовлетворяет требованиям: " +
                                  str(need) + "\n")
            if self._volume2._current_volume == need:
                self._actions += ("Кувшин 2 удовлетворяет требованиям: " +
                                  str(need) + "\n")
            if (self._volume1._current_volume == need or
                    self._volume2._current_volume == need):
                self._actions += (
                    "A* количество итераций: " +
                    str(self._iteration) +
                    "\n"
                )
                print self._actions
                return True
        else:
            return False

    def _method_a_star(self, step):
        """
        That method searches in accordance with
        as little actions for achieving result.
        First it looks at all variations with 1 action,
        after that with 2, and so on.

        """
        step += 1
        if not self._found and step < 10:
            # заполнить 1, перелить во 2
            value11 = self._volume1._current_volume
            value12 = self._volume2._current_volume
            prev_actions1 = self._actions

            self._actions += (
                "заполнить 1, перелить во 2 : " +
                "[1] : " + str(self._volume1._max_volume) + " => " +
                "[2] : " + str(self._volume2._current_volume) + "\n"
            )

            self._volume1.fill()
            can_fill = self._volume2.add(self._volume1._current_volume)
            self._volume1.add(-can_fill)

            self._iteration += 1

            if self._a_check():
                self._found = True
                return
            else:
                self._actions = prev_actions1

            # transfuse from 1 to 2
            value21 = 0
            value22 = 0
            prev_actions2 = ""

            if self._volume1._current_volume != 0:
                value21 = volume1._current_volume
                value22 = volume2._current_volume
                prev_actions2 = self._actions

                self._actions += (
                    "перелить из 1 в 2 : " +
                    "[1] : " + str(self._volume1._current_volume) + " => " +
                    "[2] : " + str(self._volume2._current_volume) + "\n"
                )

                can_fill = self._volume2.add(self._volume1._current_volume)
                self._volume1.add(-can_fill)

                self._iteration += 1

                if self._a_check():
                    self._found = True
                    return
                else:
                    self._actions = prev_actions2

            # fill 2, transfuse to 1
            value31 = self._volume1._current_volume
            value32 = self._volume2._current_volume
            prev_actions3 = self._actions

            self._actions += (
                "fill 2, transfuse to 1 : " +
                "[2] : " + str(self._volume2._max_volume) + " => " +
                "[1] : " + str(self._volume1._current_volume) + "\n"
            )

            self._volume2.fill()
            can_fill = self._volume1.add(self._volume2._current_volume)
            self._volume2.add(-can_fill)

            self._iteration += 1

            if self._a_check():
                self._found = True
                return
            else:
                self._actions = prev_actions3

            # transfuse from 2 to 1
            value41 = 0
            value42 = 0
            prev_actions4 = ""
            if self._volume2._current_volume != 0:
                value41 = self._volume1._current_volume
                value42 = self._volume2._current_volume
                prev_actions4 = self._actions

                self._actions += (
                    "перелить из 2 в 1 : " +
                    "[2] : " + str(self._volume2._current_volume) + " => " +
                    "[1] : " + str(self._volume1._current_volume) + "\n"
                )

                can_fill = self._volume1.add(self._volume2._current_volume)
                self._volume2.add(-can_fill)

                self._iteration += 1
                if self._a_check():
                    self._found = True
                    return
                else:
                    self._actions = prev_actions4

            # empty volume1
            value51 = 0
            value52 = 0
            prev_actions5 = ""

            if self._volume1._current_volume != 0:
                value51 = self._volume1._current_volume
                value52 = self._volume2._current_volume
                prev_actions5 = self._actions

                self._actions += (
                    "опустошить 1 : " +
                    "[1] : " + str(self._volume1._current_volume) +
                    "[2] : " + str(self._volume2._current_volume) + "\n"
                )

                self._volume1.empty()

                self._iteration += 1

                if self._a_check():
                    self._found = True
                    return
                else:
                    self._actions = prev_actions5

            # empty volume2
            value61 = 0
            value62 = 0
            prev_actions6 = ""

            if self._volume2._current_volume != 0:
                value61 = self._volume1._current_volume
                value62 = self._volume2._current_volume
                prev_actions6 = self._actions

                self._actions += (
                    "опустошить 2 : " +
                    "[1] : " + str(self._volume1._current_volume) +
                    "[2] : " + str(self._volume2._current_volume) + "\n"
                )

                self._volume2.empty()

                self._iteration += 1

                if self._a_check():
                    self._found = True
                    return
                self._actions = prev_actions6

            self._volume1._current_volume = value11
            self._volume2._current_volume = value12
            self._actions = prev_actions1
            self._method_a_star(step)

            if self._volume1._current_volume != 0:
                self._volume1._current_volume = value21
                self._volume2._current_volume = value22
                self._actions = prev_actions2
                self._method_a_star(step)

            self._volume1._current_volume = value31
            self._volume2._current_volume = value32
            self._actions = prev_actions3
            self._method_a_star(step)

            if self._volume2._current_volume != 0:
                self._volume1._current_volume = value41
                self._volume2._current_volume = value42
                self._actions = prev_actions4
                self._method_a_star(step)

            if self._volume1._current_volume != 0:
                self._volume1._current_volume = value51
                self._volume2._current_volume = value52
                self._actions = prev_actions5
                self._method_a_star(step)

            if self._volume2._current_volume != 0:
                self._volume1._current_volume = value61
                self._volume2._current_volume = value62
                self._actions = prev_actions6
                self._method_a_star(step)


# print volume1
# print volume2
# unit testing :D:D:D
# volume1.add(0.4)
# volume2.add(3)
# print volume1._current_volume
# print volume2._current_volume
# volume1.empty()
# volume2.empty()
# print volume1._current_volume
# print volume2._current_volume
# volume1.fill()
# volume2.fill()
# print volume1._current_volume
# print volume2._current_volume


def int_input(text):
    success = False
    int_input = 0
    while not success:
        try:
            input_string = raw_input(text)
            int_input = int(input_string)
            success = True
        except ValueError:
            print "Вы ввели не число!"
            success = False
    return int_input


volume_first = int_input("Введите емкость первого кувшина: ")
volume_second = int_input("Введите емкость второго кувшина: ")

success = False
while not success:
    need = int_input("Введите искомый объём: ")
    if volume_second > volume_first:
        if need <= volume_second:
            success = True
    else:
        if need <= volume_first:
            success = True
    if need <= 0 or not success:
        print "Введен неправильный искомый объём"
        success = False

# print volume_first, volume_second, need

volume1 = Volume(volume_first)
volume2 = Volume(volume_second)

methods = Methods(volume1, volume2, need)
methods.run_in_depth()
methods.run_a_star()
methods.run_brute_force()
